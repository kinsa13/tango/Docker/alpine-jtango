ARG BASE_IMAGE="alpine:3.15"
ARG SRC_IMAGE="kinsamanka/tango-on-alpine:0.1"
FROM $SRC_IMAGE as src

ARG JTANGO_VER="jtango-9-lts"

USER root

# Install build time dependencies
RUN apk --update add --no-cache git gradle nodejs npm maven openjdk11

# build JTango
RUN git clone --depth=1 --branch=$JTANGO_VER https://gitlab.com/tango-controls/JTango /jtango && \
    cd /jtango && \
    mvn package

WORKDIR /jtango

RUN mkdir /java && \
    sh -c "find . -type f -regex '.*/[J|T].*jar' -exec cp {} /java \;"

FROM $SRC_IMAGE

ARG BUILD_DATE
ARG GIT_REV
ARG DOCKER_TAG="0.1"

LABEL \
    org.opencontainers.image.title="alpine-jtango" \
    org.opencontainers.image.description="JTango on Alpine" \
    org.opencontainers.image.authors="GP Orcullo<kinsamanka@gmail.com>" \
    org.opencontainers.image.version=$DOCKER_TAG \
    org.opencontainers.image.url="https://hub.docker.com/repository/docker/kinsamanka/alpine-jtango" \
    org.opencontainers.image.source="https://gitlab.com/kinsa13/tango/Docker/alpine-jtango" \
    org.opencontainers.image.revision=$GIT_REV \
    org.opencontainers.image.created=$BUILD_DATE

COPY --from=src /java /usr/local/java

USER root
RUN apk  --update add --no-cache openjdk11-jre

USER tango
